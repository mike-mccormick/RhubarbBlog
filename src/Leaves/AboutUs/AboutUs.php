<?php

namespace RhubarbBlog\Leaves\AboutUs;

use Rhubarb\Leaf\Leaves\Leaf;
use RhubarbBlog\Leaves\AboutUs\AboutUsModel;

class AboutUs extends Leaf
{
    protected  function createModel()
    {
        return new AboutUsModel();
    }

    /**
     * Returns the name of the standard view used for this leaf.
     *
     * @return string
     */
    protected function getViewClass()
    {
        return AboutUsView::class;
    }
}