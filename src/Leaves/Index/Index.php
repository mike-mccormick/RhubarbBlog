<?php

namespace RhubarbBlog\Leaves\Index;
use Rhubarb\Leaf\Leaves\Leaf;

class Index extends Leaf
{
    protected function getViewClass()
    {
        return IndexView::class;
    }

    protected function createModel()
    {
        return new IndexModel();
    }
}