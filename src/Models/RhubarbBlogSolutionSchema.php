<?php

namespace RhubarbBlog\Models;

use Rhubarb\Stem\Schema\SolutionSchema;

class RhubarbBlogSolutionSchema extends SolutionSchema
{
    public function __construct($version = 0)
    {
        parent::__construct($version);
        $this->addModel("Post", Post::class);
    }
}