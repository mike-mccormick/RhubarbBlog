<?php

namespace RhubarbBlog\Layouts;

use Rhubarb\Crown\Layout\LayoutModule;
use Rhubarb\Patterns\Layouts\BaseLayout;

class DefaultLayout extends BaseLayout
{
    protected function printLayout($content)
    {
        ?>
            <html>
                <head>
                    <link rel="stylesheet" href="/static/app.css">
                </head>
            <body>
            <?php
                parent::printContent($content);
            ?>
            </body>
            </html>
        <?php
    }
}